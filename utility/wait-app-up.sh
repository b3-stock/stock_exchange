#!/bin/sh

retry=0

url_check=$1

while true;
do
    status=$(curl -s -o /dev/null -w '%{http_code}' $url_check)
    echo "curl=$url_check  status=$status"
    if [ $retry -eq 10 ]; then
        exit 1
    fi
    if [ $status -eq 200 ]; then
        echo "App status OK"
        break
    else
        curl $url_check
        retry=`expr $retry + 1`
        echo "Unable to connect app service $url_check.... retry " $retry
        sleep 5
    fi
done
