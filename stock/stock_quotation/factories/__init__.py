import random

import factory
from faker import Faker

from stock.stock_quotation.models import Company, Earnings, BalanceSheet, DemonstrationResult, Stock, Indicators

fake = Faker()


class CompanyFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Company

    code = 'FESA4'
    name = fake.name()
    paper_type = 'PN'
    sector = 'Bancario'
    sub_sector = 'Banco'


class EarningsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Earnings

    company = factory.SubFactory(CompanyFactory)
    date = fake.past_date(start_date="-30d", tzinfo=None)
    amount = round(random.random(), 4)
    type = fake.text(max_nb_chars=10, ext_word_list=None)
    multiplier = 1


class BalanceSheetFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = BalanceSheet

    company = factory.SubFactory(CompanyFactory)
    date = fake.past_date(start_date="-30d", tzinfo=None)

    ativo = round(random.random(), 4)
    disponibilidades = round(random.random(), 4)
    ativo_circulante = round(random.random(), 4)

    divida_bruta = round(random.random(), 4)
    divida_liquida = round(random.random(), 4)
    patrimonio_liquido = round(random.random(), 4)

    passivo_circulante = round(random.random(), 4)


class DemonstrationResultFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = DemonstrationResult

    company = factory.SubFactory(CompanyFactory)
    date = fake.past_date(start_date="-30d", tzinfo=None)

    receita_liquida_12_meses = round(random.random(), 4)
    resultado_bruto_12_meses = round(random.random(), 4)
    ebit_12_meses = round(random.random(), 4)
    lucro_liquido_12_meses = round(random.random(), 4)

    receita_liquida_3_meses = round(random.random(), 4)
    resultado_bruto_3_meses = round(random.random(), 4)
    ebit_3_meses = round(random.random(), 4)
    lucro_liquido_3_meses = round(random.random(), 4)


class StockFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Stock

    company = factory.SubFactory(CompanyFactory)
    date = fake.past_date(start_date="-30d", tzinfo=None)

    open = round(random.random(), 4)
    high = round(random.random(), 4)
    low = round(random.random(), 4)
    close = round(random.random(), 4)
    volume = random.randint(0, 10000)


class IndicatorsFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Indicators

    company = factory.SubFactory(CompanyFactory)
    date = fake.past_date(start_date="-30d", tzinfo=None)

    valor_de_mercado = round(random.random(), 4)
    valor_da_firma = round(random.random(), 4)
    pl = round(random.random(), 4)
    lpa = round(random.random(), 4)
    p_vp = round(random.random(), 4)
    vpa = round(random.random(), 4)
    p_ebit = round(random.random(), 4)
    margem_bruta = round(random.random(), 4)
    psr = round(random.random(), 4)
    margem_ebit = round(random.random(), 4)
    p_ativos = round(random.random(), 4)
    margem_liquida = round(random.random(), 4)
    p_cap_giro = round(random.random(), 4)
    ebit_ativo = round(random.random(), 4)
    p_ativo_circ_liquido = round(random.random(), 4)
    roic = round(random.random(), 4)
    div_yield = round(random.random(), 4)
    roe = round(random.random(), 4)
    ev_ebit = round(random.random(), 4)
    liquidez_corrente = round(random.random(), 4)
    giro_ativos = round(random.random(), 4)
    div_brut_patrim = round(random.random(), 4)
