from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_indicators(client, company):
    factories.IndicatorsFactory.create_batch(size=7, company=company)
    response = client.get(f'/api/company/{company.id}/indicators')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_list_indicators(client, company):
    factories.IndicatorsFactory.create_batch(size=7, company=company)
    response = client.get('/api/company/3/indicators')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert len(content) == 0


@pytest.mark.parametrize('indicators__date', ['2018-06-08'])
@pytest.mark.parametrize('indicators__valor_de_mercado', ['3.236'])
@pytest.mark.parametrize('indicators__valor_da_firma', ['4.556'])
@pytest.mark.parametrize('indicators__pl', ['0.123'])
@pytest.mark.parametrize('indicators__vpa', ['1256.956'])
@pytest.mark.parametrize('indicators__p_vp', ['-0.536'])
def test_should_retrieve_indicators(client, indicators):
    response = client.get(f'/api/company/{indicators.company.id}/indicators/{indicators.id}')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('date') == indicators.date
    assert content.get('company') == indicators.company.id
    assert content.get('valor_de_mercado') == indicators.valor_de_mercado
    assert content.get('valor_da_firma') == indicators.valor_da_firma
    assert content.get('pl') == indicators.pl
    assert content.get('vpa') == indicators.vpa
    assert content.get('p_vp') == indicators.p_vp


def test_should_not_retrieve_indicators(client, indicators):
    response = client.get(f'/api/company/{indicators.company.id}/indicators/5')

    assert response.status_code == HTTPStatus.NOT_FOUND


def test_should_create_indicators(client, indicators_1_as_dict):
    cpny = indicators_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/indicators',
                           data=json.dumps(indicators_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('date') == indicators_1_as_dict['date']
    assert content.get('company') == indicators_1_as_dict['company']
    assert content.get('valor_de_mercado') == indicators_1_as_dict['valor_de_mercado']
    assert content.get('valor_da_firma') == indicators_1_as_dict['valor_da_firma']
    assert content.get('pl') == indicators_1_as_dict['pl']
    assert content.get('vpa') == indicators_1_as_dict['vpa']
    assert content.get('p_vp') == indicators_1_as_dict['p_vp']


def test_should_not_create_indicators(client, indicators_1_as_dict):
    cpny = indicators_1_as_dict['company']
    del indicators_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/indicators',
                           data=json.dumps(indicators_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'company': ['This field is required.']}
