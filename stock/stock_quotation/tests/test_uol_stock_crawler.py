# from datetime import date

import pytest

from stock.stock_quotation.stock_service import UOLStockCrawler

# TODO: add requests mock


@pytest.fixture
def crawler():
    return UOLStockCrawler()


# def test_should_get_url(crawler):
#     code = 'PETR4'
#     url = crawler._get_url(code)
#     assert url == 'http://cotacoes.economia.uol.com.br/acao/cotacoes-historicas.html?codigo={}.SA&beginDay=1' \
#                   '&beginMonth=1&beginYear=2000&endDay={}&endMonth={}&endYear={}&size=4000&page=1'\
#         .format(code, date.today().day, date.today().month, date.today().year)
#
#
# def test_should_get_table(crawler):
#     crawler.get_stocks('PETR4')
