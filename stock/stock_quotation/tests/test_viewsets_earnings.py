from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_earnings(client, company):
    factories.EarningsFactory.create_batch(size=7, company=company)
    response = client.get(f'/api/company/{company.id}/earnings')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_list_earnings(client, company):
    factories.EarningsFactory.create_batch(size=7, company=company)
    response = client.get('/api/company/3/earnings')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert len(content) == 0


@pytest.mark.parametrize('earnings__amount', ['123.0000'])
@pytest.mark.parametrize('earnings__type', ['Dividendos'])
@pytest.mark.parametrize('earnings__multiplier', [1])
def test_should_retrieve_earnings(client, earnings):
    response = client.get(f'/api/company/{earnings.company.id}/earnings/{earnings.id}')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('amount') == str(earnings.amount)
    assert content.get('type') == earnings.type
    assert content.get('company') == earnings.company.id
    assert content.get('multiplier') == earnings.multiplier


def test_should_not_retrieve_earnings(client, earnings):
    response = client.get(f'/api/company/{earnings.company.id}/earnings/5')

    assert response.status_code == HTTPStatus.NOT_FOUND


def test_should_create_earnings(client, earnings_1_as_dict):
    cpny = earnings_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/earnings',
                           data=json.dumps(earnings_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('amount') == str(earnings_1_as_dict['amount'])
    assert content.get('type') == earnings_1_as_dict['type']
    assert content.get('company') == earnings_1_as_dict['company']
    assert content.get('multiplier') == earnings_1_as_dict['multiplier']


def test_should_not_create_earnings(client, earnings_1_as_dict):
    cpny = earnings_1_as_dict['company']
    del earnings_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/earnings',
                           data=json.dumps(earnings_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'company': ['This field is required.']}
