from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories
from stock.stock_quotation.models import Company
from stock.stock_quotation.serializers import CompanySerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_companies(client):
    factories.CompanyFactory.create_batch(size=7)
    response = client.get('/api/company/')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_retrieve_companies(client):
    factories.CompanyFactory.create_batch(size=7)
    last_company = Company.objects.last()
    response = client.get(f'/api/company/{last_company.id+1}/')

    assert response.status_code == HTTPStatus.NOT_FOUND
    content = json.loads(response.content.decode())
    assert content == {'detail': 'Not found.'}


@pytest.mark.parametrize('company__code', ['FESA4'])
@pytest.mark.parametrize('company__name', ['Ferbasa'])
@pytest.mark.parametrize('company__paper_type', ['ON'])
@pytest.mark.parametrize('company__sector', ['Banco'])
@pytest.mark.parametrize('company__sub_sector', ['FinTech'])
def test_should_retrieve_company(client, company):
    factories.CompanyFactory.create_batch(size=3)
    first_company = Company.objects.first()
    response = client.get(f'/api/company/{first_company.id}/')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('code') == company.code
    assert content.get('name') == company.name
    assert content.get('paper_type') == company.paper_type
    assert content.get('sector') == company.sector
    assert content.get('sub_sector') == company.sub_sector


def test_should_destroy_company(client):
    factories.CompanyFactory.create_batch(size=3)
    company = Company.objects.first()
    assert company.enabled is True
    response = client.delete(f'/api/company/{company.id}/')

    assert response.status_code == HTTPStatus.OK
    company = Company.objects.first()
    assert company.enabled is False


def test_should_not_destroy_company(client):
    factories.CompanyFactory.create_batch(size=3)
    company = Company.objects.last()
    response = client.delete(f'/api/company/{company.id + 1}/')

    assert response.status_code == HTTPStatus.NOT_FOUND
    assert response.status_text == 'Not Found'


def test_should_create_company(client, company_1_as_dict):
    response = client.post('/api/company/', data=json.dumps(company_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('code') == company_1_as_dict['code']
    assert content.get('name') == company_1_as_dict['name']
    assert content.get('paper_type') == company_1_as_dict['paper_type']
    assert content.get('sector') == company_1_as_dict['sector']
    assert content.get('sub_sector') == company_1_as_dict['sub_sector']


def test_should_not_create_company(client, company_1_as_dict):
    del company_1_as_dict['code']
    response = client.post('/api/company/', data=json.dumps(company_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'code': ['This field is required.']}


@pytest.mark.parametrize('company__code', ['FESA4'])
@pytest.mark.parametrize('company__name', ['Ferbasa'])
def test_should_update_company(client, company):
    serialized_company = CompanySerializer(company).data
    serialized_company['name'] = 'Ferbasa mod'
    response = client.put(f'/api/company/{company.id}/',
                          data=json.dumps(serialized_company), content_type='application/json')
    assert response.status_code == HTTPStatus.OK
    updated_company = Company.objects.get(id=company.id)
    assert updated_company.name == 'Ferbasa mod'
