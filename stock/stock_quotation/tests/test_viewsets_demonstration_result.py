from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories
from stock.stock_quotation.models import DemonstrationResult

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_demonstration_result(client, company):
    factories.DemonstrationResultFactory.create_batch(size=7, company=company)
    assert DemonstrationResult.objects.count() == 7
    response = client.get(f'/api/company/{company.id}/demonstration_result')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_list_demonstration_result(client, company):
    factories.DemonstrationResultFactory.create_batch(size=7, company=company)
    response = client.get('/api/company/3/demonstration_result')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert len(content) == 0


@pytest.mark.parametrize('demonstration_result__date', ['2018-08-10'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_12_meses', ['12.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_12_meses', ['11.00'])
@pytest.mark.parametrize('demonstration_result__ebit_12_meses', ['10.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_12_meses', ['9.00'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_3_meses', ['5.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_3_meses', ['4.00'])
@pytest.mark.parametrize('demonstration_result__ebit_3_meses', ['3.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_3_meses', ['2.00'])
def test_should_retrieve_demonstration_result(client, demonstration_result):
    url = f'/api/company/{demonstration_result.company.id}/demonstration_result/{demonstration_result.id}'
    response = client.get(url)

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('date') == demonstration_result.date
    assert content.get('company') is not None
    assert content.get('receita_liquida_12_meses') == demonstration_result.receita_liquida_12_meses
    assert content.get('resultado_bruto_12_meses') == demonstration_result.resultado_bruto_12_meses
    assert content.get('ebit_12_meses') == demonstration_result.ebit_12_meses
    assert content.get('lucro_liquido_12_meses') == demonstration_result.lucro_liquido_12_meses

    assert content.get('receita_liquida_3_meses') == demonstration_result.receita_liquida_3_meses
    assert content.get('resultado_bruto_3_meses') == demonstration_result.resultado_bruto_3_meses
    assert content.get('ebit_3_meses') == demonstration_result.ebit_3_meses
    assert content.get('lucro_liquido_3_meses') == demonstration_result.lucro_liquido_3_meses


def test_should_not_retrieve_demonstration_result(client, demonstration_result):
    response = client.get(f'/api/company/{demonstration_result.company.id}/demonstration_result/5')

    assert response.status_code == HTTPStatus.NOT_FOUND


def test_should_create_demonstration_result(client, demonstration_result_1_as_dict):
    cpny = demonstration_result_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/demonstration_result',
                           data=json.dumps(demonstration_result_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('date') == demonstration_result_1_as_dict['date']
    assert content.get('company') is not None
    assert content.get('receita_liquida_12_meses') == demonstration_result_1_as_dict['receita_liquida_12_meses']
    assert content.get('resultado_bruto_12_meses') == demonstration_result_1_as_dict['resultado_bruto_12_meses']
    assert content.get('ebit_12_meses') == demonstration_result_1_as_dict['ebit_12_meses']
    assert content.get('lucro_liquido_12_meses') == demonstration_result_1_as_dict['lucro_liquido_12_meses']

    assert content.get('receita_liquida_3_meses') == demonstration_result_1_as_dict['receita_liquida_3_meses']
    assert content.get('resultado_bruto_3_meses') == demonstration_result_1_as_dict['resultado_bruto_3_meses']
    assert content.get('ebit_3_meses') == demonstration_result_1_as_dict['ebit_3_meses']
    assert content.get('lucro_liquido_3_meses') == demonstration_result_1_as_dict['lucro_liquido_3_meses']


def test_should_not_create_demonstration_result(client, demonstration_result_1_as_dict):
    cpny = demonstration_result_1_as_dict['company']
    del demonstration_result_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/demonstration_result',
                           data=json.dumps(demonstration_result_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'company': ['This field is required.']}
