import datetime
from decimal import Decimal

import pytest

from stock.stock_quotation.models import Earnings
from stock.stock_quotation.serializers import EarningsSerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('earnings__amount', ['0.2943'])
@pytest.mark.parametrize('earnings__type', ['JRS CAP PROPRIO'])
@pytest.mark.parametrize('earnings__multiplier', [1])
@pytest.mark.parametrize('earnings__date', ['2018-06-08'])
def test_should_serialize_earnings(earnings):
    serializer = EarningsSerializer(earnings)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('amount') == '0.2943'
    assert serializer.data.get('type') == 'JRS CAP PROPRIO'
    assert serializer.data.get('multiplier') == 1
    assert serializer.data.get('date') == '2018-06-08'
    assert serializer.data.get('company') is not None


def test_should_deserialize_earnings(earnings_1_as_dict):

    serializer = EarningsSerializer(data=earnings_1_as_dict)

    assert serializer.is_valid()

    earnings = serializer.save()
    assert isinstance(earnings, Earnings)
    assert earnings.id
    assert earnings.amount == Decimal('0.2943')
    assert earnings.type == 'JRS CAP PROPRIO'
    assert earnings.multiplier == 1
    assert earnings.date == datetime.date(2018, 6, 6)


def test_should_not_deserialize_earnings(earnings_1_as_dict):
    del earnings_1_as_dict['company']
    serializer = EarningsSerializer(data=earnings_1_as_dict)

    assert not serializer.is_valid()
