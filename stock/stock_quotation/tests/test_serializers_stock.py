import datetime
from decimal import Decimal

import pytest

from stock.stock_quotation.models import Stock
from stock.stock_quotation.serializers import StockSerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('stock__open', ['0.294'])
@pytest.mark.parametrize('stock__high', ['1.000'])
@pytest.mark.parametrize('stock__low', ['0.200'])
@pytest.mark.parametrize('stock__close', ['0.250'])
@pytest.mark.parametrize('stock__volume', [10000])
@pytest.mark.parametrize('stock__date', ['2018-06-08'])
def test_should_serialize_stock(stock):
    serializer = StockSerializer(stock)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('date') == stock.date
    assert serializer.data.get('company') is not None

    assert serializer.data.get('open') == stock.open
    assert serializer.data.get('high') == stock.high
    assert serializer.data.get('low') == stock.low

    assert serializer.data.get('close') == stock.close
    assert serializer.data.get('volume') == stock.volume


def test_should_deserialize_stock(stock_1_as_dict):
    serializer = StockSerializer(data=stock_1_as_dict)

    assert serializer.is_valid()

    stock_result = serializer.save()
    assert isinstance(stock_result, Stock)
    assert stock_result.id
    assert stock_result.date == datetime.date(2018, 6, 7)

    assert stock_result.open == Decimal('0.294')
    assert stock_result.high == Decimal('0.300')
    assert stock_result.low == Decimal('0.2')

    assert stock_result.close == Decimal('0.25')
    assert stock_result.volume == 10000


def test_should_not_deserialize_stock(stock_1_as_dict):
    del stock_1_as_dict['company']
    serializer = StockSerializer(data=stock_1_as_dict)

    assert not serializer.is_valid()
