import datetime
from decimal import Decimal

import pytest

from stock.stock_quotation.models import DemonstrationResult
from stock.stock_quotation.serializers import DemonstrationResultSerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('demonstration_result__date', ['2018-08-10'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_12_meses', ['12.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_12_meses', ['11.00'])
@pytest.mark.parametrize('demonstration_result__ebit_12_meses', ['10.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_12_meses', ['9.00'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_3_meses', ['5.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_3_meses', ['4.00'])
@pytest.mark.parametrize('demonstration_result__ebit_3_meses', ['3.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_3_meses', ['2.00'])
def test_should_serialize_demonstration_result(demonstration_result):
    serializer = DemonstrationResultSerializer(demonstration_result)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('date') == demonstration_result.date
    assert serializer.data.get('company') is not None
    assert serializer.data.get('receita_liquida_12_meses') == demonstration_result.receita_liquida_12_meses
    assert serializer.data.get('resultado_bruto_12_meses') == demonstration_result.resultado_bruto_12_meses
    assert serializer.data.get('ebit_12_meses') == demonstration_result.ebit_12_meses
    assert serializer.data.get('lucro_liquido_12_meses') == demonstration_result.lucro_liquido_12_meses

    assert serializer.data.get('receita_liquida_3_meses') == demonstration_result.receita_liquida_3_meses
    assert serializer.data.get('resultado_bruto_3_meses') == demonstration_result.resultado_bruto_3_meses
    assert serializer.data.get('ebit_3_meses') == demonstration_result.ebit_3_meses
    assert serializer.data.get('lucro_liquido_3_meses') == demonstration_result.lucro_liquido_3_meses


def test_should_deserialize_demonstration_result(demonstration_result_1_as_dict):
    serializer = DemonstrationResultSerializer(data=demonstration_result_1_as_dict)

    assert serializer.is_valid()

    demonstration_result = serializer.save()
    assert isinstance(demonstration_result, DemonstrationResult)
    assert demonstration_result.id
    assert demonstration_result.date == datetime.date(2018, 6, 7)
    assert demonstration_result.receita_liquida_12_meses == Decimal('12.00')
    assert demonstration_result.resultado_bruto_12_meses == Decimal('11.00')
    assert demonstration_result.ebit_12_meses == Decimal('10.00')
    assert demonstration_result.lucro_liquido_12_meses == Decimal('9.00')

    assert demonstration_result.receita_liquida_3_meses == Decimal('5.00')
    assert demonstration_result.resultado_bruto_3_meses == Decimal('4.00')
    assert demonstration_result.ebit_3_meses == Decimal('3.00')
    assert demonstration_result.lucro_liquido_3_meses == Decimal('2.00')


def test_should_not_deserialize_demonstration_result(demonstration_result_1_as_dict):
    del demonstration_result_1_as_dict['company']
    serializer = DemonstrationResultSerializer(data=demonstration_result_1_as_dict)

    assert not serializer.is_valid()
