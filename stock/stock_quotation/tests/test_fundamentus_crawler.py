

# TODO: improve tests
from datetime import date


def test_should_get_companies_list(list_crawler):
    companies_list = list_crawler.get_company_list()
    assert len(companies_list) == 867


def test_should_get_fundamentus(details_crawler):
    company = details_crawler.get_fundamentus(paper='FESA4')

    indicators = company.get('indicators')
    assert len(indicators) == 1
    assert indicators[0].get('market_value') == 1607420000.0
    assert indicators[0].get('valor_da_firma') == 1125410000.0

    earnings = company.get('earnings')
    assert len(earnings) == 41
    assert earnings[0]['date'] == date(year=2018, month=6, day=7)
    assert earnings[0]['amount'] == 0.2943
    assert earnings[0]['type'] == 'JRS CAP PROPRIO'
    assert earnings[0]['multiplier'] == 1
