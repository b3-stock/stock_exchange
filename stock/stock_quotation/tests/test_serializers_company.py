import pytest

from stock.stock_quotation.models import Company
from stock.stock_quotation.serializers import CompanySerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('company__code', ['FESA4'])
@pytest.mark.parametrize('company__name', ['Ferbasa'])
@pytest.mark.parametrize('company__paper_type', ['ON'])
@pytest.mark.parametrize('company__sector', ['Banco'])
@pytest.mark.parametrize('company__sub_sector', ['FinTech'])
def test_should_serialize_company(company):
    serializer = CompanySerializer(company)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('code') == company.code
    assert serializer.data.get('name') == company.name
    assert serializer.data.get('paper_type') == company.paper_type
    assert serializer.data.get('sector') == company.sector
    assert serializer.data.get('sub_sector') == company.sub_sector


def test_should_deserialize_company(company_1_as_dict):
    serializer = CompanySerializer(data=company_1_as_dict)

    assert serializer.is_valid()

    company = serializer.save()
    assert isinstance(company, Company)
    assert company.id
    assert company.code == 'FESA4'
    assert company.name == 'FESA'
    assert company.paper_type == 'PN'
    assert company.sector == 'Banking'
    assert company.sub_sector == 'FinTech'
