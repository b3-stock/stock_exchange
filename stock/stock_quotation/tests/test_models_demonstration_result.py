from datetime import date

import pytest
from django.db import IntegrityError

from stock.stock_quotation.models import DemonstrationResult

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('demonstration_result__date', ['2018-08-10'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_12_meses', ['12.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_12_meses', ['11.00'])
@pytest.mark.parametrize('demonstration_result__ebit_12_meses', ['10.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_12_meses', ['9.00'])
@pytest.mark.parametrize('demonstration_result__receita_liquida_3_meses', ['5.00'])
@pytest.mark.parametrize('demonstration_result__resultado_bruto_3_meses', ['4.00'])
@pytest.mark.parametrize('demonstration_result__ebit_3_meses', ['3.00'])
@pytest.mark.parametrize('demonstration_result__lucro_liquido_3_meses', ['2.00'])
def test_should_create_demonstration_result(demonstration_result):
    assert DemonstrationResult.objects.count() == 1
    assert demonstration_result.date == '2018-08-10'
    assert demonstration_result.receita_liquida_12_meses == '12.00'
    assert demonstration_result.resultado_bruto_12_meses == '11.00'
    assert demonstration_result.ebit_12_meses == '10.00'
    assert demonstration_result.lucro_liquido_12_meses == '9.00'
    assert demonstration_result.receita_liquida_3_meses == '5.00'
    assert demonstration_result.resultado_bruto_3_meses == '4.00'
    assert demonstration_result.ebit_3_meses == '3.00'
    assert demonstration_result.lucro_liquido_3_meses == '2.00'


def test_should_not_create_demonstration_result_without_company():
    dr = DemonstrationResult(date=date(year=2018, month=6, day=7),
                             receita_liquida_12_meses=12, resultado_bruto_12_meses=12, ebit_12_meses=12,
                             lucro_liquido_12_meses=12, receita_liquida_3_meses=3, resultado_bruto_3_meses=3,
                             ebit_3_meses=3, lucro_liquido_3_meses=3)
    with pytest.raises(IntegrityError):
        dr.save()
