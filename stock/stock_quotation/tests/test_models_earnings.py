from datetime import date

import pytest
from django.db import IntegrityError

from stock.stock_quotation.models import Earnings

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('earnings__amount', ['0.2943'])
@pytest.mark.parametrize('earnings__type', ['JRS CAP PROPRIO'])
@pytest.mark.parametrize('earnings__multiplier', [1])
@pytest.mark.parametrize('earnings__date', ['2018-06-08'])
def test_should_create_earnings(earnings):
    assert Earnings.objects.count() == 1
    assert earnings.date == '2018-06-08'
    assert earnings.amount == '0.2943'
    assert earnings.type == 'JRS CAP PROPRIO'
    assert earnings.multiplier == 1


def test_should_not_create_earnings_without_company():
    ea = Earnings(date=date(year=2018, month=6, day=7), amount=0.2943, type='JRS CAP PROPRIO',
                  multiplier=1)
    with pytest.raises(IntegrityError):
        ea.save()
