import pytest

from stock.stock_quotation.models import Company

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('company__code', ['FESA4'])
@pytest.mark.parametrize('company__name', ['Ferbasa'])
@pytest.mark.parametrize('company__paper_type', ['PN'])
@pytest.mark.parametrize('company__sector', ['Bancario'])
@pytest.mark.parametrize('company__sub_sector', ['Banco'])
def test_should_create_company(company):
    assert Company.objects.count() == 1
    assert company.code == 'FESA4'
    assert company.name == 'Ferbasa'
    assert company.paper_type == 'PN'
    assert company.sector == 'Bancario'
    assert company.sub_sector == 'Banco'
