from datetime import date

import pytest
from django.db import IntegrityError

from stock.stock_quotation.models import Indicators

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('indicators__date', ['2018-06-08'])
@pytest.mark.parametrize('indicators__valor_de_mercado', ['3.236'])
@pytest.mark.parametrize('indicators__valor_da_firma', ['4.556'])
@pytest.mark.parametrize('indicators__pl', ['0.123'])
@pytest.mark.parametrize('indicators__vpa', ['1256.956'])
@pytest.mark.parametrize('indicators__p_vp', ['-0.536'])
def test_should_create_indicators(indicators):
    assert Indicators.objects.count() == 1

    assert indicators.date == '2018-06-08'
    assert indicators.valor_de_mercado == '3.236'
    assert indicators.valor_da_firma == '4.556'
    assert indicators.pl == '0.123'
    assert indicators.vpa == '1256.956'
    assert indicators.p_vp == '-0.536'


def test_should_not_create_indicators_without_company():
    st = Indicators(date=date(year=2018, month=6, day=7), valor_de_mercado=0.2943, valor_da_firma=0.2943,
                    pl=0.2943, lpa=0.2943, p_vp=0.2943, vpa=0.2943, p_ebit=0.2943, margem_bruta=0.2943, psr=0.2943,
                    margem_ebit=0.2943, p_ativos=0.2943, margem_liquida=0.2943, p_cap_giro=0.2943, ebit_ativo=0.2943,
                    p_ativo_circ_liquido=0.2943, roic=0.2943, div_yield=0.2943, roe=0.2943, ev_ebit=0.2943,
                    liquidez_corrente=0.2943, giro_ativos=0.2943, div_brut_patrim=0.2943)
    with pytest.raises(IntegrityError):
        st.save()
