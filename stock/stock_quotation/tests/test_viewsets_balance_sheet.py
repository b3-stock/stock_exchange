from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories
from stock.stock_quotation.models import BalanceSheet

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_balance_sheet(client, company):
    factories.BalanceSheetFactory.create_batch(size=7, company=company)
    assert BalanceSheet.objects.count() == 7
    response = client.get(f'/api/company/{company.id}/balance_sheet')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_list_balance_sheet(client, company):
    factories.BalanceSheetFactory.create_batch(size=7, company=company)
    response = client.get('/api/company/3/balance_sheet')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert len(content) == 0


@pytest.mark.parametrize('balance_sheet__date', ['2018-08-10'])
@pytest.mark.parametrize('balance_sheet__ativo', ['12.00'])
@pytest.mark.parametrize('balance_sheet__disponibilidades', ['11.00'])
@pytest.mark.parametrize('balance_sheet__ativo_circulante', ['10.00'])
@pytest.mark.parametrize('balance_sheet__divida_bruta', ['9.00'])
@pytest.mark.parametrize('balance_sheet__divida_liquida', ['8.00'])
@pytest.mark.parametrize('balance_sheet__patrimonio_liquido', ['7.00'])
@pytest.mark.parametrize('balance_sheet__passivo_circulante', ['6.00'])
def test_should_retrieve_balance_sheet(client, balance_sheet):
    response = client.get(f'/api/company/{balance_sheet.company.id}/balance_sheet/{balance_sheet.id}')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('date') == balance_sheet.date
    assert content.get('company') is not None
    assert content.get('ativo') == balance_sheet.ativo
    assert content.get('disponibilidades') == balance_sheet.disponibilidades
    assert content.get('ativo_circulante') == balance_sheet.ativo_circulante

    assert content.get('divida_bruta') == balance_sheet.divida_bruta
    assert content.get('divida_liquida') == balance_sheet.divida_liquida
    assert content.get('patrimonio_liquido') == balance_sheet.patrimonio_liquido
    assert content.get('passivo_circulante') == balance_sheet.passivo_circulante


def test_should_not_retrieve_balance_sheet(client, balance_sheet):
    response = client.get(f'/api/company/{balance_sheet.company.id}/balance_sheet/5')

    assert response.status_code == HTTPStatus.NOT_FOUND


def test_should_create_balance_sheet(client, balance_sheet_1_as_dict):
    cpny = balance_sheet_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/balance_sheet',
                           data=json.dumps(balance_sheet_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('date') == balance_sheet_1_as_dict['date']
    assert content.get('company') is not None
    assert content.get('ativo') == balance_sheet_1_as_dict['ativo']
    assert content.get('disponibilidades') == balance_sheet_1_as_dict['disponibilidades']
    assert content.get('ativo_circulante') == balance_sheet_1_as_dict['ativo_circulante']

    assert content.get('divida_bruta') == balance_sheet_1_as_dict['divida_bruta']
    assert content.get('divida_liquida') == balance_sheet_1_as_dict['divida_liquida']
    assert content.get('patrimonio_liquido') == balance_sheet_1_as_dict['patrimonio_liquido']
    assert content.get('passivo_circulante') == balance_sheet_1_as_dict['passivo_circulante']


def test_should_not_create_balance_sheet(client, balance_sheet_1_as_dict):
    cpny = balance_sheet_1_as_dict['company']
    del balance_sheet_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/balance_sheet',
                           data=json.dumps(balance_sheet_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'company': ['This field is required.']}
