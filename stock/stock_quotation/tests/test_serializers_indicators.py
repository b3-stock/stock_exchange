import datetime
from decimal import Decimal

import pytest

from stock.stock_quotation.models import Indicators
from stock.stock_quotation.serializers import IndicatorsSerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('indicators__date', ['2018-06-08'])
@pytest.mark.parametrize('indicators__valor_de_mercado', ['3.236'])
@pytest.mark.parametrize('indicators__valor_da_firma', ['4.556'])
@pytest.mark.parametrize('indicators__pl', ['0.123'])
@pytest.mark.parametrize('indicators__vpa', ['1256.956'])
@pytest.mark.parametrize('indicators__p_vp', ['-0.536'])
def test_should_serialize_indicators(indicators):
    serializer = IndicatorsSerializer(indicators)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('date') == '2018-06-08'
    assert serializer.data.get('valor_de_mercado') == '3.236'
    assert serializer.data.get('valor_da_firma') == '4.556'
    assert serializer.data.get('pl') == '0.123'
    assert serializer.data.get('vpa') == '1256.956'
    assert serializer.data.get('p_vp') == '-0.536'
    assert serializer.data.get('company') is not None


def test_should_deserialize_indicators(indicators_1_as_dict):

    serializer = IndicatorsSerializer(data=indicators_1_as_dict)

    assert serializer.is_valid()

    indicators = serializer.save()
    assert isinstance(indicators, Indicators)
    assert indicators.id
    assert indicators.valor_de_mercado == Decimal('20.010')
    assert indicators.valor_da_firma == Decimal('20.020')
    assert indicators.pl == Decimal('20.030')
    assert indicators.vpa == Decimal('20.060')
    assert indicators.p_vp == Decimal('20.050')
    assert indicators.date == datetime.date(2018, 6, 7)


def test_should_not_deserialize_indicators(indicators_1_as_dict):
    del indicators_1_as_dict['company']
    serializer = IndicatorsSerializer(data=indicators_1_as_dict)

    assert not serializer.is_valid()
