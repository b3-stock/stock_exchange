from datetime import date

import pytest
from django.db import IntegrityError

from stock.stock_quotation.models import BalanceSheet

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('balance_sheet__date', ['2018-08-10'])
@pytest.mark.parametrize('balance_sheet__ativo', ['12.00'])
@pytest.mark.parametrize('balance_sheet__disponibilidades', ['11.00'])
@pytest.mark.parametrize('balance_sheet__ativo_circulante', ['10.00'])
@pytest.mark.parametrize('balance_sheet__divida_bruta', ['9.00'])
@pytest.mark.parametrize('balance_sheet__divida_liquida', ['8.00'])
@pytest.mark.parametrize('balance_sheet__patrimonio_liquido', ['7.00'])
@pytest.mark.parametrize('balance_sheet__passivo_circulante', ['6.00'])
def test_should_create_balance_sheet(balance_sheet):
    assert BalanceSheet.objects.count() == 1
    assert balance_sheet.date == '2018-08-10'
    assert balance_sheet.ativo == '12.00'
    assert balance_sheet.disponibilidades == '11.00'
    assert balance_sheet.ativo_circulante == '10.00'
    assert balance_sheet.divida_bruta == '9.00'
    assert balance_sheet.divida_liquida == '8.00'
    assert balance_sheet.patrimonio_liquido == '7.00'
    assert balance_sheet.passivo_circulante == '6.00'


def test_should_not_create_balance_sheet_without_company():
    bs = BalanceSheet(date=date(year=2018, month=6, day=7),
                      ativo=12, disponibilidades=11, ativo_circulante=10,
                      divida_bruta=9, divida_liquida=8, patrimonio_liquido=7,
                      passivo_circulante=6)
    with pytest.raises(IntegrityError):
        bs.save()
