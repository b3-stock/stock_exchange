import datetime
from decimal import Decimal

import pytest

from stock.stock_quotation.models import BalanceSheet
from stock.stock_quotation.serializers import BalanceSheetSerializer

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('balance_sheet__date', ['2018-08-10'])
@pytest.mark.parametrize('balance_sheet__ativo', ['12.00'])
@pytest.mark.parametrize('balance_sheet__disponibilidades', ['11.00'])
@pytest.mark.parametrize('balance_sheet__ativo_circulante', ['10.00'])
@pytest.mark.parametrize('balance_sheet__divida_bruta', ['9.00'])
@pytest.mark.parametrize('balance_sheet__divida_liquida', ['8.00'])
@pytest.mark.parametrize('balance_sheet__patrimonio_liquido', ['7.00'])
@pytest.mark.parametrize('balance_sheet__passivo_circulante', ['6.00'])
def test_should_serialize_balance_sheet(balance_sheet):
    serializer = BalanceSheetSerializer(balance_sheet)

    assert isinstance(serializer.data, dict)
    assert serializer.data.get('date') == balance_sheet.date
    assert serializer.data.get('company') is not None
    assert serializer.data.get('ativo') == balance_sheet.ativo
    assert serializer.data.get('disponibilidades') == balance_sheet.disponibilidades
    assert serializer.data.get('ativo_circulante') == balance_sheet.ativo_circulante

    assert serializer.data.get('divida_bruta') == balance_sheet.divida_bruta
    assert serializer.data.get('divida_liquida') == balance_sheet.divida_liquida
    assert serializer.data.get('patrimonio_liquido') == balance_sheet.patrimonio_liquido
    assert serializer.data.get('passivo_circulante') == balance_sheet.passivo_circulante


def test_should_deserialize_balance_sheet(balance_sheet_1_as_dict):
    serializer = BalanceSheetSerializer(data=balance_sheet_1_as_dict)

    assert serializer.is_valid()

    balance_sheet_result = serializer.save()
    assert isinstance(balance_sheet_result, BalanceSheet)
    assert balance_sheet_result.id
    assert balance_sheet_result.date == datetime.date(2018, 6, 7)

    assert balance_sheet_result.ativo == Decimal('12.00')
    assert balance_sheet_result.disponibilidades == Decimal('11.00')
    assert balance_sheet_result.ativo_circulante == Decimal('10.00')

    assert balance_sheet_result.divida_bruta == Decimal('9.00')
    assert balance_sheet_result.divida_liquida == Decimal('8.00')
    assert balance_sheet_result.patrimonio_liquido == Decimal('7.00')
    assert balance_sheet_result.passivo_circulante == Decimal('6.00')


def test_should_not_deserialize_balance_sheet(balance_sheet_1_as_dict):
    del balance_sheet_1_as_dict['company']
    serializer = BalanceSheetSerializer(data=balance_sheet_1_as_dict)

    assert not serializer.is_valid()
