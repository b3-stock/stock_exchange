from datetime import date

import pytest
from django.db import IntegrityError

from stock.stock_quotation.models import Stock

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


@pytest.mark.parametrize('stock__open', ['0.294'])
@pytest.mark.parametrize('stock__high', ['1'])
@pytest.mark.parametrize('stock__low', ['0.2'])
@pytest.mark.parametrize('stock__close', ['0.25'])
@pytest.mark.parametrize('stock__volume', ['10000'])
@pytest.mark.parametrize('stock__date', ['2018-06-08'])
def test_should_create_stock(stock):
    assert Stock.objects.count() == 1

    assert stock.open == '0.294'
    assert stock.high == '1'
    assert stock.low == '0.2'
    assert stock.close == '0.25'
    assert stock.volume == '10000'
    assert stock.date == '2018-06-08'


def test_should_not_create_stock_without_company():
    st = Stock(date=date(year=2018, month=6, day=7), open=2, close=3, low=2, high=3, volume=200)
    with pytest.raises(IntegrityError):
        st.save()
