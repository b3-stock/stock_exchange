from http import HTTPStatus

import pytest
from rest_framework.utils import json

from stock.stock_quotation import factories

pytestmark = [pytest.mark.django_db, pytest.mark.serial]


def test_should_list_all_stocks(client, company):
    factories.StockFactory.create_batch(size=7, company=company)
    response = client.get(f'/api/company/{company.id}/stock')

    assert response.status_code == HTTPStatus.OK

    content = json.loads(response.content.decode())
    assert len(content) == 7


def test_should_not_list_stocks(client, company):
    factories.StockFactory.create_batch(size=7, company=company)
    response = client.get('/api/company/3/stock')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert len(content) == 0


@pytest.mark.parametrize('stock__open', ['0.294'])
@pytest.mark.parametrize('stock__high', ['1.000'])
@pytest.mark.parametrize('stock__low', ['0.200'])
@pytest.mark.parametrize('stock__close', ['0.250'])
@pytest.mark.parametrize('stock__volume', [10000])
@pytest.mark.parametrize('stock__date', ['2018-06-08'])
def test_should_retrieve_stock(client, stock):
    response = client.get(f'/api/company/{stock.company.id}/stock/{stock.id}')

    assert response.status_code == HTTPStatus.OK
    content = json.loads(response.content.decode())
    assert isinstance(content, dict)
    assert content.get('date') == stock.date
    assert content.get('company') == stock.company.id
    assert content.get('open') == stock.open
    assert content.get('high') == stock.high
    assert content.get('low') == stock.low
    assert content.get('close') == stock.close
    assert content.get('volume') == stock.volume


def test_should_not_retrieve_stock(client, stock):
    response = client.get(f'/api/company/{stock.company.id}/stock/5')

    assert response.status_code == HTTPStatus.NOT_FOUND


def test_should_create_stock(client, stock_1_as_dict):
    cpny = stock_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/stock',
                           data=json.dumps(stock_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.CREATED

    content = json.loads(response.content.decode())
    assert content.get('id') is not None
    assert content.get('date') == stock_1_as_dict['date']
    assert content.get('company') == stock_1_as_dict['company']
    assert content.get('open') == stock_1_as_dict['open']
    assert content.get('high') == stock_1_as_dict['high']
    assert content.get('low') == stock_1_as_dict['low']
    assert content.get('close') == stock_1_as_dict['close']
    assert content.get('volume') == stock_1_as_dict['volume']


def test_should_not_create_stock(client, stock_1_as_dict):
    cpny = stock_1_as_dict['company']
    del stock_1_as_dict['company']
    response = client.post(f'/api/company/{cpny}/stock',
                           data=json.dumps(stock_1_as_dict), content_type='application/json')
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert json.loads(response.content.decode()) == {'company': ['This field is required.']}
