
# http://www.bmfbovespa.com.br/pt_br/produtos/listados-a-vista-e-derivativos/renda-variavel/empresas-listadas.htm
from datetime import datetime

import requests

import re

from bs4 import BeautifulSoup
from lxml.html import fragment_fromstring
from lxml import html


BASE_URL = 'http://www.fundamentus.com.br/'


class FundamentusCrawler:
    base_url = 'http://www.fundamentus.com.br/'

    def __init__(self):
        self.companies = []

        self.earnings_table_id = 'resultado'
        self.earnings = [{'date': lambda v: datetime.strptime(v, '%d/%m/%Y').date()},
                         {'amount': lambda v: self._clear_value(v)},
                         {'type': lambda v: v},
                         {'multiplier': lambda v: int(v)}]

    @property
    def titles(self):
        return ['Nro. Ações', 'P/L', 'LPA', 'P/VP', 'VPA', 'P/EBIT', 'Marg. Bruta', 'PSR', 'Marg. EBIT', 'P/Ativos',
                'Marg. Líquida', 'P/Cap. Giro', 'EBIT / Ativo', 'P/Ativ Circ Liq', 'ROIC', 'Div. Yield', 'ROE',
                'EV / EBIT', 'Liquidez Corr', 'Giro Ativos', 'Div Br/ Patrim', 'Cres. Rec (5a)', 'Ativo', 'Dív. Bruta',
                'Disponibilidades', 'Dív. Líquida', 'Ativo Circulante', 'Patrim. Líq', 'Receita Líquida', 'EBIT',
                'Lucro Líquido']

    @staticmethod
    def _clear_value(value):
        transformed_value = value.strip('\t\r\n% ').replace('.', '').replace(',', '.')
        if transformed_value == '-':
            return 0
        return float(transformed_value)

    def validate_title(self, title):
        # find a way to validate the title and get the value automatically
        pass

    @staticmethod
    def _get_result_release_date(tree):
        print('validating report release age...')
        release_date_parsed = tree.xpath('//table[2]//tr[1]//td[4]/span[1]/text()')
        if not len(release_date_parsed):
            return None
        release_date = datetime.strptime(release_date_parsed[0], '%d/%m/%Y')
        # # if release_date + timedelta(days=365) < datetime.today():
        # #     return None
        return release_date.date()

    @staticmethod
    def _get_last_negotiation_date(tree):
        last_negotiation_date_parsed = tree.xpath('//table[1]//tr[2]//td[4]/span[1]/text()')
        if not len(last_negotiation_date_parsed) or last_negotiation_date_parsed[0] == '-':
            return None
        last_negotiation_date = datetime.strptime(last_negotiation_date_parsed[0], '%d/%m/%Y')
        return last_negotiation_date.date()

    def _get_indicators(self, tree, release_date):
        indicators = {
            # 2 table
            'release_date': release_date,

            'stock_no': self._clear_value(tree.xpath('//table[2]//tr[2]//td[4]/span[1]/text()')[0]),
            'market_value': self._clear_value(tree.xpath('//table[2]//tr[1]//td[2]/span[1]/text()')[0]),
            'valor_da_firma': self._clear_value(tree.xpath('//table[2]//tr[2]//td[2]/span[1]/text()')[0]),

            # indicadores fundamentalistas
            'pl': self._clear_value(tree.xpath('//table[3]//tr[2]//td[4]/span[1]/text()')[0]),
            'lpa': self._clear_value(tree.xpath('//table[3]//tr[2]//td[6]/span[1]/text()')[0]),
            'pvp': self._clear_value(tree.xpath('//table[3]//tr[3]//td[4]/span[1]/text()')[0]),
            'vpa': self._clear_value(tree.xpath('//table[3]//tr[3]//td[6]/span[1]/text()')[0]),
            'pebit': self._clear_value(tree.xpath('//table[3]//tr[4]//td[4]/span[1]/text()')[0]),
            'mb': self._clear_value(tree.xpath('//table[3]//tr[4]//td[6]/span[1]/text()')[0]),
            'psr': self._clear_value(tree.xpath('//table[3]//tr[5]//td[4]/span[1]/text()')[0]),
            'me': self._clear_value(tree.xpath('//table[3]//tr[5]//td[6]/span[1]/text()')[0]),
            'at': self._clear_value(tree.xpath('//table[3]//tr[6]//td[4]/span[1]/text()')[0]),
            'ml': self._clear_value(tree.xpath('//table[3]//tr[6]//td[6]/span[1]/text()')[0]),
            'pcg': self._clear_value(tree.xpath('//table[3]//tr[7]//td[4]/span[1]/text()')[0]),
            'ebita': self._clear_value(tree.xpath('//table[3]//tr[7]//td[6]/span[1]/text()')[0]),
            'p_ativ_circ_liq': self._clear_value(tree.xpath('//table[3]//tr[8]//td[4]/span[1]/text()')[0]),
            'roic': self._clear_value(tree.xpath('//table[3]//tr[8]//td[6]/span[1]/text()')[0]),
            'div_yield': self._clear_value(tree.xpath('//table[3]//tr[9]//td[4]/span[1]/text()')[0]),
            'roe': self._clear_value(tree.xpath('//table[3]//tr[9]//td[6]/span[1]/text()')[0]),
            'ev_ebit': self._clear_value(tree.xpath('//table[3]//tr[10]//td[4]/span[1]/text()')[0]),
            'liquidez_corr': self._clear_value(tree.xpath('//table[3]//tr[10]//td[6]/span[1]/text()')[0]),
            'giro_ativ': self._clear_value(tree.xpath('//table[3]//tr[11]//td[4]/span[1]/text()')[0]),
            'div_br_patrim': self._clear_value(tree.xpath('//table[3]//tr[11]//td[6]/span[1]/text()')[0]),
            'cresc_rec_5_a': self._clear_value(tree.xpath('//table[3]//tr[12]//td[4]/span[1]/text()')[0]),

            # balanco patrimonial
            'ativo': self._clear_value(tree.xpath('//table[4]//tr[2]//td[2]/span[1]/text()')[0]),
            'div_bruta': self._clear_value(tree.xpath('//table[4]//tr[2]//td[4]/span[1]/text()')[0]),
            'disponibilidades': self._clear_value(tree.xpath('//table[4]//tr[3]//td[2]/span[1]/text()')[0]),
            'div_liquida': self._clear_value(tree.xpath('//table[4]//tr[3]//td[4]/span[1]/text()')[0]),
            'ativo_circulante': self._clear_value(tree.xpath('//table[4]//tr[4]//td[2]/span[1]/text()')[0]),
            'patr_liquido': self._clear_value(tree.xpath('//table[4]//tr[4]//td[4]/span[1]/text()')[0]),

            # demonstrativo de resultados
            'receita_liquida_12_meses': self._clear_value(tree.xpath('//table[5]//tr[3]//td[2]/span[1]/text()')[0]),
            'ebit_12_meses': self._clear_value(tree.xpath('//table[5]//tr[4]//td[2]/span[1]/text()')[0]),
            'lucro_liquido_12_meses': self._clear_value(tree.xpath('//table[5]//tr[5]//td[2]/span[1]/text()')[0]),
            'receita_liquida_3_meses': self._clear_value(tree.xpath('//table[5]//tr[3]//td[4]/span[1]/text()')[0]),
            'ebit_3_meses': self._clear_value(tree.xpath('//table[5]//tr[4]//td[4]/span[1]/text()')[0]),
            'lucro_liquido_3_meses': self._clear_value(tree.xpath('//table[5]//tr[5]//td[4]/span[1]/text()')[0])
        }
        return indicators

    @staticmethod
    def _sanitize_paper_type(value: str):
        if 'ON' in value:
            return 'ON'
        if 'PN' in value:
            return 'PN'
        if 'PNA' in value:
            return 'PNA'
        return '-'

    def _get_base_details(self, tree):
        paper_type = tree.xpath('//table[1]//tr[2]//td[2]/span[1]/text()')
        name = tree.xpath('//table[1]//tr[3]//td[2]/span[1]/text()')
        sector = tree.xpath('//table[1]//tr[4]//td[2]/span[1]/a/text()')
        sub_sector = tree.xpath('//table[1]//tr[5]//td[2]/span[1]/a/text()')
        # first table
        company = {
            'code': tree.xpath('//table[1]//tr[1]//td[2]/span[1]/text()')[0],
            'paper_type': self._sanitize_paper_type(paper_type[0]) if paper_type else '-',
            'name': name[0] if name else '-',
            'sector': sector[0] if sector else '-',
            'sub_sector': sub_sector[0] if sub_sector else '-',
            'last_negotiation_date': self._get_last_negotiation_date(tree)
        }
        return company

    def _get_fundamentus_detail(self, paper='FESA4'):
        print('crawling fundamentus from {}'.format(paper))
        url = '{}detalhes.php?papel={}'.format(BASE_URL, paper)
        page = requests.get(url)
        tree = html.fromstring(page.text)

        release_date = self._get_result_release_date(tree)
        if release_date is None:
            return None

        company = self._get_base_details(tree)
        indicators = self._get_indicators(tree, release_date)

        company['indicators'] = [indicators]
        return company

    def get_fundamentus(self, paper='FESA4'):
        company = self._get_fundamentus_detail(paper=paper)
        earnings = self.get_earnings(paper=paper)

        company['earnings'] = earnings
        return company

    def get_companies_fundamentus_detail(self, papers=None):
        if papers is None:
            papers = []

        companies = []

        for paper in papers:
            company = self.get_fundamentus(paper.code)
            companies.append(company)

        return companies

    @staticmethod
    def get_company_list():
        content = requests.get('{}resultado.php'.format(BASE_URL))
        pattern = re.compile('<table id="resultado".*</table>', re.DOTALL)
        reg = re.findall(pattern, content.text)[0]
        page = fragment_fromstring(reg)
        lista = []

        for rows in page.xpath('tbody')[0].findall("tr"):
            children = rows.getchildren()
            name = children[0][0].getchildren()[0].text
            data = {'code': name}
            lista.append(data)
        return lista

    def _get_earnings_url(self, paper):
        url = '{}proventos.php?papel={}'.format(self.base_url, paper)
        return url

    def get_earnings(self, paper='FESA4'):
        print('crawling earnings from {}'.format(paper))
        url = self._get_earnings_url(paper)
        response = requests.get(url)
        response.raise_for_status()
        tree = BeautifulSoup(response.text, 'lxml')
        return self._parse_earnings_page(tree)

    def _parse_earnings_page(self, tree):
        table = tree.find('table', id=self.earnings_table_id)
        rows = table.findAll('tr')
        earnings_list = []

        for row in rows:
            cells = row.find_all('td')
            earnings = {}
            for title, cell_value in zip(self.earnings, cells):
                # TODO: fix this line, find a better way
                earnings[next(iter(title))] = next(iter(title.values()))(cell_value.get_text())
            if earnings:
                earnings_list.append(earnings)
        return earnings_list
