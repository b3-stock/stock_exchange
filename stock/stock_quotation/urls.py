from django.urls import path, include
from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()

router.register(r'company', views.CompanyViewSet)
# router.register(r'order_company', views.OrderCompanyViewSet)
# router.register(r'stock', views.StockViewSet)

earnings_router = routers.NestedSimpleRouter(router, r'company', lookup='company', trailing_slash=False)
earnings_router.register(r'earnings', views.EarningsViewSet, base_name='earnings')

balance_sheet_router = routers.NestedSimpleRouter(router, r'company', lookup='company', trailing_slash=False)
balance_sheet_router.register(r'balance_sheet', views.BalanceSheetViewSet, base_name='balance_sheet')

demonstration_result_router = routers.NestedSimpleRouter(router, r'company', lookup='company', trailing_slash=False)
demonstration_result_router.register(r'demonstration_result', views.DemonstrationResultViewSet,
                                     base_name='demonstration_result')

stock_router = routers.NestedSimpleRouter(router, r'company', lookup='company', trailing_slash=False)
stock_router.register(r'stock', views.StockViewSet, base_name='stock')

indicators_router = routers.NestedSimpleRouter(router, r'company', lookup='company', trailing_slash=False)
indicators_router.register(r'indicators', views.IndicatorsViewSet, base_name='indicators')

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
app_name = 'stock_quotation'
urlpatterns = [
    path('', include(earnings_router.urls)),
    path('', include(balance_sheet_router.urls)),
    path('', include(demonstration_result_router.urls)),
    path('', include(stock_router.urls)),
    path('', include(indicators_router.urls)),
]

urlpatterns += router.urls
