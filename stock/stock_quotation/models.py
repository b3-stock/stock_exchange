# import datetime

from django.db import models


class Company(models.Model):
    code = models.CharField("Code", max_length=10, blank=False, null=False)
    name = models.CharField("Name", max_length=50, blank=True)
    paper_type = models.CharField("Paper Type", max_length=2, blank=True)
    sector = models.CharField("Sector", max_length=50, blank=True)
    sub_sector = models.CharField("Sub Sector", max_length=100, blank=True)
    last_negotiation_date = models.DateField(auto_now=False, auto_now_add=False, blank=True, null=True)
    enabled = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    updated_at = models.DateTimeField(auto_now=True, editable=False)


class Earnings(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='earnings')
    date = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    amount = models.DecimalField(max_digits=15, decimal_places=4)
    type = models.CharField("Type", max_length=50, blank=True)
    multiplier = models.IntegerField(default=1)

    class Meta:
        get_latest_by = 'date'
        ordering = ('date', )


class DemonstrationResult(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='demonstration_result')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    date = models.DateField(auto_now=False, auto_now_add=False)

    receita_liquida_12_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    resultado_bruto_12_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    ebit_12_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    lucro_liquido_12_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)

    receita_liquida_3_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    resultado_bruto_3_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    ebit_3_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    lucro_liquido_3_meses = models.DecimalField(max_digits=30, decimal_places=2, default=0)

    class Meta:
        get_latest_by = 'date'


class BalanceSheet(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='balance_sheet')
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    date = models.DateField(auto_now=False, auto_now_add=False)

    ativo = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    disponibilidades = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    ativo_circulante = models.DecimalField(max_digits=30, decimal_places=2, default=0)

    divida_bruta = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    divida_liquida = models.DecimalField(max_digits=30, decimal_places=2, default=0)
    patrimonio_liquido = models.DecimalField(max_digits=30, decimal_places=2, default=0)

    passivo_circulante = models.DecimalField(max_digits=30, decimal_places=2, default=0)

    class Meta:
        get_latest_by = 'date'


class Stock(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='stock')
    date = models.DateField(auto_now=False, auto_now_add=False)

    open = models.DecimalField(max_digits=15, decimal_places=3)
    high = models.DecimalField(max_digits=15, decimal_places=3)
    low = models.DecimalField(max_digits=15, decimal_places=3)
    close = models.DecimalField(max_digits=15, decimal_places=3)
    volume = models.PositiveIntegerField(default=0)

    class Meta:
        get_latest_by = 'date'


class Indicators(models.Model):
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='indicators')
    date = models.DateField(auto_now=False, auto_now_add=False)

    valor_de_mercado = models.DecimalField(max_digits=15, decimal_places=3)
    valor_da_firma = models.DecimalField(max_digits=15, decimal_places=3)
    pl = models.DecimalField(max_digits=15, decimal_places=3)
    lpa = models.DecimalField(max_digits=15, decimal_places=3)
    p_vp = models.DecimalField(max_digits=15, decimal_places=3)
    vpa = models.DecimalField(max_digits=15, decimal_places=3)
    p_ebit = models.DecimalField(max_digits=15, decimal_places=3)
    margem_bruta = models.DecimalField(max_digits=15, decimal_places=3)
    psr = models.DecimalField(max_digits=15, decimal_places=3)
    margem_ebit = models.DecimalField(max_digits=15, decimal_places=3)
    p_ativos = models.DecimalField(max_digits=15, decimal_places=3)
    margem_liquida = models.DecimalField(max_digits=15, decimal_places=3)
    p_cap_giro = models.DecimalField(max_digits=15, decimal_places=3)
    ebit_ativo = models.DecimalField(max_digits=15, decimal_places=3)
    p_ativo_circ_liquido = models.DecimalField(max_digits=15, decimal_places=3)
    roic = models.DecimalField(max_digits=15, decimal_places=3)
    div_yield = models.DecimalField(max_digits=15, decimal_places=3)
    roe = models.DecimalField(max_digits=15, decimal_places=3)
    ev_ebit = models.DecimalField(max_digits=15, decimal_places=3)
    liquidez_corrente = models.DecimalField(max_digits=15, decimal_places=3)
    giro_ativos = models.DecimalField(max_digits=15, decimal_places=3)
    div_brut_patrim = models.DecimalField(max_digits=15, decimal_places=3)
