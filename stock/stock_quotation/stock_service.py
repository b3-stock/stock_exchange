import os
from datetime import date
import requests
from bs4 import BeautifulSoup


class AlphaAdvantageStock:
    api_key = os.getenv('ALPHA_ADVANTAGE_API_KEY', 'demo')
    base_url = 'https://www.alphavantage.co/'

    def __init__(self):
        print('Using {} key.'.format(self.api_key))

    @staticmethod
    def _parse_data(data):
        stocks = list()
        error = data.get('Error Message', None)
        if error is None:
            series = data.get('Time Series (Daily)', None)
            if series is not None:
                for key, value in series.items():
                    stock = {
                        'date': key,
                        'open': value.get('1. open')[:-1],
                        'high': value.get('2. high')[:-1],
                        'low': value.get('3. low')[:-1],
                        'close': value.get('4. close')[:-1],
                        'volume': value.get('5. volume')
                    }
                    stocks.append(stock)
            return stocks, 'ok'
        return [], error

    def get_daily_series(self, symbol, output_size='compact'):
        url = self.base_url + 'query?function=TIME_SERIES_DAILY&symbol={}.SA&outputsize={}&apikey={}'
        url = url.format(symbol, output_size, self.api_key)
        response = requests.get(url)
        if response.status_code == 200:
            data, msg = self._parse_data(response.json())
            return data, msg
        return [], 'empty'


class UOLStockCrawler:
    base_url = 'http://cotacoes.economia.uol.com.br/acao/cotacoes-historicas.html'
    table_id = 'tblInterday'

    def __init__(self, begin_day=None, begin_month=None, begin_year=None,
                 end_day=None, end_month=None, end_year=None):
        self.begin_day = begin_day or 1
        self.begin_month = begin_month or 1
        self.begin_year = begin_year or 2000
        self.end_day = end_day or date.today().day
        self.end_month = end_month or date.today().month
        self.end_year = end_year or date.today().year

    def _get_url(self, code):
        size = 4000
        page = 1
        url = '{}?codigo={}.SA&beginDay={}&beginMonth={}&beginYear={}&endDay={}&endMonth={}&endYear={}&size={}&page={}'\
            .format(self.base_url, code, self.begin_day, self.begin_month, self.begin_year,
                    self.end_day, self.end_month, self.end_year, size, page)
        return url

    def get_stocks(self, code='FESA4'):
        url = self._get_url(code)
        response = requests.get(url)
        response.raise_for_status()
        tree = BeautifulSoup(response.text, 'lxml')
        self.parse_page(tree)

    # TODO: SANITIZE VALUES
    def parse_page(self, tree):
        table = tree.find('table', id=self.table_id)
        rows = table.findAll('tr')
        for row in rows:
            cells = row.find_all('td')
            if cells:
                stock_date = cells[0].get_text()
                close = cells[1].get_text()
                open = '0'
                low = cells[2].get_text()
                high = cells[3].get_text()
                volume = cells[6].get_text()
                print('{}: {}: {}: {}: {}: {}'.format(stock_date, open, close, low, high, volume))
