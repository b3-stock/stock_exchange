# from stock.stock_quotation.models import FundamentalistIndicator, OrderCompany
#
#
# def get_company_grade(indicator: FundamentalistIndicator, order: OrderCompany):  # noqa: C901
#     grade = 0
#     if not order.disable_pl:  # inversamente
#         if indicator.pl:
#             grade += (1 / indicator.pl) * order.pl
#     if not order.disable_lpa:
#         grade += indicator.lpa * order.lpa
#     if not order.disable_pvp:
#         if indicator.pvp:   # inversamente
#             grade += (1 / indicator.pvp) * order.pvp
#     if not order.disable_vpa:
#         grade += indicator.vpa * order.vpa
#     if not order.disable_pebit:
#         grade += indicator.pebit * order.pebit
#     if not order.disable_mb:
#         grade += indicator.mb * order.mb
#     if not order.disable_psr:
#         grade += indicator.psr * order.psr
#     if not order.disable_me:
#         grade += indicator.me * order.me
#     if not order.disable_at:
#         grade += indicator.at * order.at
#     if not order.disable_ml:
#         grade += indicator.ml * order.ml
#     if not order.disable_pcg:
#         grade += indicator.pcg * order.pcg
#     if not order.disable_ebita:
#         grade += indicator.ebita * order.ebita
#     if not order.disable_p_ativ_circ_liq:
#         grade += indicator.p_ativ_circ_liq * order.p_ativ_circ_liq
#     if not order.disable_roic:
#         grade += indicator.roic * order.roic
#     if not order.disable_div_yield:
#         grade += indicator.div_yield * order.div_yield
#     if not order.disable_roe:
#         grade += indicator.roe * order.roe
#     if not order.disable_ev_ebit:
#         grade += indicator.ev_ebit * order.ev_ebit
#     if not order.disable_liquidez_corr:
#         grade += indicator.liquidez_corr * order.liquidez_corr
#     if not order.disable_giro_ativ:
#         grade += indicator.giro_ativ * order.giro_ativ
#     if not order.disable_div_br_patrim:
#         grade += indicator.div_br_patrim * order.div_br_patrim
#     if not order.disable_cresc_rec_5_a:
#         grade += indicator.cresc_rec_5_a * order.cresc_rec_5_a
#     if not order.disable_ativo:
#         grade += indicator.ativo * order.ativo
#     if not order.disable_div_bruta:
#         grade += indicator.div_bruta * order.div_bruta
#     if not order.disable_disponibilidades:
#         grade += indicator.disponibilidades * order.disponibilidades
#     if not order.disable_div_liquida:
#         grade += indicator.div_liquida * order.div_liquida
#     if not order.disable_ativo_circulante:
#         grade += indicator.ativo_circulante * order.ativo_circulante
#     if not order.disable_patr_liquido:
#         grade += indicator.patr_liquido * order.patr_liquido
#     return grade
