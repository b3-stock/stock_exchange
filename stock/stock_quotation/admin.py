from django.contrib import admin
from .models import Company


class CompanyAdmin(admin.ModelAdmin):
    pass


class FundamentalistIndicatorAdmin(admin.ModelAdmin):
    pass


class OrderCompanyAdmin(admin.ModelAdmin):
    pass


admin.site.register(Company, CompanyAdmin)
# admin.site.register(FundamentalistIndicator, FundamentalistIndicatorAdmin)
# admin.site.register(OrderCompany, OrderCompanyAdmin)
