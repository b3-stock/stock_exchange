from rest_framework import serializers

from .models import Company, Stock, Earnings, DemonstrationResult, BalanceSheet, Indicators


class EarningsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Earnings
        fields = '__all__'
        read_only_fields = ('id',)

    def create(self, validated_data):
        date = validated_data.get('date')
        earnings_type = validated_data.get('type')
        company = validated_data.get('company')
        if not Earnings.objects.filter(date=date, company=company, type__icontains=earnings_type).exists():
            earnings = Earnings.objects.create(**validated_data)
            return earnings
        return validated_data


# class CompanyCodeSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Company
#         fields = ('code',)
#
#     def create(self, validated_data):
#         company_in_db = Company.objects.filter(code=validated_data.get('code')).first()
#         if not company_in_db:
#             Company.objects.create(**validated_data)


class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = Stock
        fields = '__all__'
        # fields = ('id', 'date', 'open', 'high', 'low', 'close', 'volume')

    # def create(self, validated_data):
    #     date = validated_data.get('date')
    #     company = validated_data.get('company')
    #     if not Stock.objects.filter(date=date, company=company).exists():
    #         stock = Stock.objects.create(**validated_data)
    #         return stock
    #     return validated_data


class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = '__all__'


class DemonstrationResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = DemonstrationResult
        fields = '__all__'


class BalanceSheetSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceSheet
        fields = '__all__'


class IndicatorsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Indicators
        fields = '__all__'
