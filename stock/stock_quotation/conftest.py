import os

import pytest
from django.test import Client
from pytest_factoryboy import register

from stock.stock_quotation.crawler_service import FundamentusCrawler

from stock.stock_quotation.factories import CompanyFactory, EarningsFactory, BalanceSheetFactory, \
    DemonstrationResultFactory, StockFactory, IndicatorsFactory

register(CompanyFactory)  # fixture name is company
register(EarningsFactory)  # fixture name is earnings
register(BalanceSheetFactory)  # fixture name is balance_sheet
register(DemonstrationResultFactory)  # fixture name is demonstration_result
register(StockFactory)  # fixture name is stock
register(IndicatorsFactory)  # fixture name is indicators


@pytest.fixture
def client():
    return Client()


@pytest.fixture
def fundamentus_mock_list_data():
    filename = os.path.join(os.path.dirname(__file__), 'tests', 'resources',
                            'fundamentus_list-utf8.html')
    file = open(filename, 'r')
    return file.read()


@pytest.fixture
def fundamentus_mock_details_data():
    filename = os.path.join(os.path.dirname(__file__), 'tests', 'resources',
                            'fundamentus_details_fesa4-utf8.html')
    file = open(filename, 'r')
    return file.read()


@pytest.fixture
def fundamentus_mock_proventos_data():
    filename = os.path.join(os.path.dirname(__file__), 'tests', 'resources',
                            'fundamentus_proventos_fesa4-utf8.html')
    file = open(filename, 'r')
    return file.read()


@pytest.fixture
def list_crawler(requests_mock, fundamentus_mock_list_data):
    requests_mock.get('http://www.fundamentus.com.br/resultado.php',
                      text=fundamentus_mock_list_data,
                      status_code=200)
    return FundamentusCrawler()


@pytest.fixture
def details_crawler(requests_mock, fundamentus_mock_details_data, fundamentus_mock_proventos_data):
    requests_mock.get('http://www.fundamentus.com.br/detalhes.php?papel=FESA4',
                      text=fundamentus_mock_details_data, status_code=200)
    requests_mock.get('http://www.fundamentus.com.br/proventos.php?papel=FESA4',
                      text=fundamentus_mock_proventos_data, status_code=200)
    return FundamentusCrawler()


@pytest.fixture
def company_1_as_dict():
    return {
        'code': 'FESA4',
        'name': 'FESA',
        'paper_type': 'PN',
        'sector': 'Banking',
        'sub_sector': 'FinTech'
    }


@pytest.fixture
def earnings_1_as_dict(company):
    return {
        'date': '2018-06-06',
        'amount': 0.2943,
        'type': 'JRS CAP PROPRIO',
        'multiplier': 1,
        'company': company.id
    }


@pytest.fixture
def balance_sheet_1_as_dict(company):
    return {
        'date': '2018-06-07',
        'company': company.id,
        'ativo': '12.00',
        'disponibilidades': '11.00',
        'ativo_circulante': '10.00',

        'divida_bruta': '9.00',
        'divida_liquida': '8.00',
        'patrimonio_liquido': '7.00',
        'passivo_circulante': '6.00',
    }


@pytest.fixture
def demonstration_result_1_as_dict(company):
    return {
        'date': '2018-06-07',
        'company': company.id,
        'receita_liquida_12_meses': '12.00',
        'resultado_bruto_12_meses': '11.00',
        'ebit_12_meses': '10.00',
        'lucro_liquido_12_meses': '9.00',

        'receita_liquida_3_meses': '5.00',
        'resultado_bruto_3_meses': '4.00',
        'ebit_3_meses': '3.00',
        'lucro_liquido_3_meses': '2.00',
    }


@pytest.fixture
def stock_1_as_dict(company):
    return {
        'date': '2018-06-07',
        'company': company.id,
        'high': '0.300',
        'open': '0.294',
        'stock': '1',
        'low': '0.200',
        'close': '0.250',
        'volume': 10000
    }


@pytest.fixture
def indicators_1_as_dict(company):
    return {
        'date': '2018-06-07',
        'company': company.id,
        'valor_de_mercado': '20.010',
        'valor_da_firma': '20.020',
        'pl': '20.030',
        'lpa': '20.040',
        'p_vp': '20.050',
        'vpa': '20.060',
        'p_ebit': '20.070',
        'margem_bruta': '20.080',
        'psr': '20.090',
        'margem_ebit': '20.100',
        'p_ativos': '20.110',
        'margem_liquida': '20.120',
        'p_cap_giro': '20.130',
        'ebit_ativo': '20.140',
        'p_ativo_circ_liquido': '20.150',
        'roic': '20.160',
        'div_yield': '20.170',
        'roe': '20.180',
        'ev_ebit': '20.190',
        'liquidez_corrente': '20.200',
        'giro_ativos': '20.210',
        'div_brut_patrim': '20.220'
    }
