# Stock Exchange


## Pre-requisites

* Python 3.6.6
* SQlite or Postgresql
* Make

Using Makefile to describe the possible tasks used in development.

## Setup 

# Set-up Virtual Environment

The following commands install and set-up `pyenv` tool (https://github.com/pyenv/pyenv) used to create/manage virtual environments:

```bash
$ git clone https://github.com/pyenv/pyenv.git ~/.pyenv
$ echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
$ echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshrc
$ echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n eval "$(pyenv init -)"\nfi' >> ~/.zshrc
$ exec "$SHELL"
$ git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
$ echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.zshrc
$ exec "$SHELL"


Clone the repository in a local folder.
```bash
$ make setup-dev
```


## Running test

```bash
make test
```

## Running app

```bash
make run
```
Access your browser at http://localhost:5000.
