FROM python:3.6.5-alpine

EXPOSE 8080

WORKDIR /app

ENV PORT=8080 \
    HOME=/app \
    PATH=/app/.local/bin/:$PATH

COPY . /app

RUN apk update && \
    apk add --no-cache --virtual .build-deps make git libc-dev gcc g++ postgresql-dev libxslt-dev && \
    apk add libxslt bash libpq && \
    make setup && \
    chmod 775 -R /app && \
    apk del .build-deps

CMD ./run.sh
