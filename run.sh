#!/bin/bash

#Valor default é execução web.
if [ -z "$PROCESS_TYPE" ]; then
    PROCESS_TYPE="web"
fi

#Captura a linha de execução de migração de dados.
line_migrate=$(grep migrate: Procfile)

#Caso existir linha de comando para migração de dados, a mesma é executada.
if [ ! -z line_migrate ]; then
    command_migrate=${line_migrate#*:}
    echo "Running <migrate>: $command_migrate"
    eval $command_migrate
fi

echo "Process type selected: $PROCESS_TYPE"

#Captura a linha de execução no Procfile.
line_command=$(grep $PROCESS_TYPE: Procfile)
command=${line_command#*:}

echo "Running <$PROCESS_TYPE>: $command"
eval $command
