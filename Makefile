.DEFAULT_GOAL := default_target

PIP := pip install --no-cache-dir -r
APP :=

PROJECT_NAME := stock_exchange
PYTHON_VERSION := 3.6.6
VENV_NAME := $(PROJECT_NAME)-$(PYTHON_VERSION)

DATABASE_NAME := stockdb
DATABASE_USER := stockuser
DATABASE_PASS := teste1234
DATABASE_URL := $(if $(DATABASE_URL),$(DATABASE_URL),postgres://$(DATABASE_USER):$(DATABASE_PASS)@localhost:5432/$(DATABASE_NAME))

# Environment setup
.pip:
	pip install pip --upgrade

setup: .pip
	$(PIP) requirements.txt

setup-dev: .pip
	$(PIP) requirements-dev.txt

.clean-build: ## remove build artifacts
	rm -fr build/
	rm -fr dist/
	rm -fr .eggs/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -f {} +

.clean-pyc: ## remove Python file artifacts
	find . -name '*.pyc' -exec rm -f {} +
	find . -name '*.pyo' -exec rm -f {} +
	find . -name '*~' -exec rm -f {} +
	find . -name '__pycache__' -exec rm -fr {} +

.clean-test: ## remove test and coverage artifacts
	rm -fr .tox/
	rm -f .coverage
	rm -fr htmlcov/
	rm -fr reports/
	rm -fr .pytest_cache/

clean: .clean-build .clean-pyc .clean-test ## remove all build, test, coverage and Python artifacts

.create-venv:
	pyenv install -s $(PYTHON_VERSION)
	pyenv uninstall -f $(VENV_NAME)
	pyenv virtualenv $(PYTHON_VERSION) $(VENV_NAME)
	pyenv local $(VENV_NAME)

create-venv: .create-venv setup-dev

# Database
migrations:
	python manage.py makemigrations $(APP)

migrations-clean:
	rm -rf stock/stock_quotation/migrations

db-up:
	python manage.py migrate

collectstatic:
	python manage.py collectstatic --noinput

code-convention:
	flake8
	pycodestyle

test:
	py.test --cov-report=term-missing  --cov-report=html --cov=.

run:
	python manage.py runserver 0.0.0.0:8000

# Docker

run-database:
	docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD='$(DATABASE_PASS)' -e POSTGRES_USER=$(DATABASE_USER) -e POSTGRES_DB=$(DATABASE_NAME) -d postgres:10.4-alpine

default_target: clean code-convention test
